const {response} = require("express");
const Usuario = require('../models/usuario');
const bcryptjs = require('bcryptjs');
const generarJWT = require("../helpers/generarJWT.JS");
const  login = async(req,res = response)=>{
    const {correo,password} = req.body;
    try {
        //verificar si el correo existe
        const usuario = await Usuario.findOne({correo});
        if(!usuario){
            return res.status(400).json({
                msg:'Usuario / Password no son correctos -Correo'
            })
        }
        //Si el usuario esta activo
        if(!usuario.estado){
            return res.status(400).json({
                msg:'Usuario / Password no son correctos -Correo estado: false'
            })
        }

        //Verificar la contraseña
        const salt = bcryptjs.genSaltSync(10);
        const david = bcryptjs.hashSync( password, salt );
        const validPassword = bcryptjs.compareSync(password,usuario.password);
        if(!validPassword){
            return res.status(400).json({
                msg:'password incorrecto',
                pss:david
            })
        }
        //Generar el el JWT
        const token = await generarJWT(usuario.id);
        res.json({
            usuario,
            token
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:"Hable con el administrador"
        })
    }
    
}

module.exports ={
    login
}