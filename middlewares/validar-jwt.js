const { response, request } = require('express');
const jwt = require('jsonwebtoken');
const Usuario = require('../models/usuario')
const validarJWT = async(req = request, res = response,next) =>{
    const token = req.header('x-token');
    if(!token){
        return res.status(401).json({
            msg:'No hay token en la peticon'
        });
    }
    try {
        //console.log(process.env.SECRETORPRIVATE);
        const { uid } = jwt.verify(token,process.env.SECRETORPRIVATE);
        // leer el usuario que corresponde al uid
        const usuario = await Usuario.findById(uid);
        req.usuario = usuario;
        if(!usuario ){
            return res.status(401).json({
                msg:'Token no valido -usuario no existe'
            })
        }
        // verificar si el uid tiene el estado en true
        if(!usuario.estado){
        return res.status(401).json({
            msg:'Token no valido -usuario con estado: false'
        })
        }
        next();
    } catch (error) {
        console.log(error);
        res.status(401).json({
            msg:"Token no valido"
        })
    }
    console.log(token);
    
}
module.exports = {
    validarJWT
}