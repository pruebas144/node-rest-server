const express = require('express');
const cors = require('cors');

const { dbConnection } = require('../database/config');

class Server {
    //Incializa el servidor
    constructor() {

        this.app  = express();
        this.port = process.env.PORT;
        //LA RUTA ES IMPORTANTE
        this.usuariosPath = '/api/usuarios';
        //Autenticacion
        this.authpath = '/api/auth';

        // Conectar a base de datos
        this.conectarDB();

        // Middlewares
        this.middlewares();

        // Rutas de mi aplicación
        this.routes();
    }

    async conectarDB() {
        await dbConnection();
    }


    middlewares() {

        // CORS
        this.app.use( cors() );

        // Lectura y parseo del body
        this.app.use( express.json() );

        // Directorio Público donde se encuentran archivos 
        this.app.use( express.static('public') );

    }

    routes() {
        this.app.use( this.authpath, require('../routes/auth'));
        this.app.use( this.usuariosPath, require('../routes/usuarios'));
    }

    listen() {
        this.app.listen( this.port, () => {
            console.log('Servidor corriendo en puerto', this.port );
        });
    }

}




module.exports = Server;
